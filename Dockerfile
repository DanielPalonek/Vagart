FROM node:10-alpine

WORKDIR /usr/src/app

COPY angular.json ./
COPY dist/ /usr/src/app/dist

COPY . .

CMD [ "sh", "-c", "npm run now-start"]
