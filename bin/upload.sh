#!/bin/bash

node ./node_modules/angular2-image-gallery/convert.js ./src/assets/img/grafika/papeteria/banery --outputDir ./src/assets/img/gallery/banners --gName=banners
cp -r ./src/assets/img/gallery/banners ./dist/app/browser/assets/img/gallery/banners
node ./node_modules/angular2-image-gallery/convert.js ./src/assets/img/grafika/papeteria/bileciki_na_kolacz --outputDir ./src/assets/img/gallery/cakeTickets --gName=cakeTickets
cp -r ./src/assets/img/gallery/cakeTickets ./dist/app/browser/assets/img/gallery/cakeTickets
node ./node_modules/angular2-image-gallery/convert.js ./src/assets/img/grafika/papeteria/dekoracje --outputDir ./src/assets/img/gallery/decoration --gName=decoration
cp -r ./src/assets/img/gallery/decoration ./dist/app/browser/assets/img/gallery/decoration
node ./node_modules/angular2-image-gallery/convert.js ./src/assets/img/grafika/papeteria/koszyczek_ratunkowy --outputDir ./src/assets/img/gallery/helpTicket --gName=helpTicket
cp -r ./src/assets/img/gallery/helpTicket ./dist/app/browser/assets/img/gallery/helpTicket
node ./node_modules/angular2-image-gallery/convert.js ./src/assets/img/grafika/papeteria/ksiega_gosci --outputDir ./src/assets/img/gallery/guestBook --gName=guestBook
cp -r ./src/assets/img/gallery/guestBook ./dist/app/browser/assets/img/gallery/guestBook
node ./node_modules/angular2-image-gallery/convert.js ./src/assets/img/grafika/papeteria/numeracja_stolikow --outputDir ./src/assets/img/gallery/table_numeration --gName=table_numeration
cp -r ./src/assets/img/gallery/table_numeration ./dist/app/browser/assets/img/gallery/table_numeration
node ./node_modules/angular2-image-gallery/convert.js ./src/assets/img/grafika/papeteria/menu --outputDir ./src/assets/img/gallery/menu --gName=menu
cp -r ./src/assets/img/gallery/menu ./dist/app/browser/assets/img/gallery/menu
node ./node_modules/angular2-image-gallery/convert.js ./src/assets/img/grafika/papeteria/plakaty --outputDir ./src/assets/img/gallery/poster --gName=poster
cp -r ./src/assets/img/gallery/poster ./dist/app/browser/assets/img/gallery/poster
node ./node_modules/angular2-image-gallery/convert.js ./src/assets/img/grafika/papeteria/plan_usadzenia_gosci --outputDir ./src/assets/img/gallery/sitPlan --gName=sitPlan
cp -r ./src/assets/img/gallery/sitPlan ./dist/app/browser/assets/img/gallery/sitPlan
node ./node_modules/angular2-image-gallery/convert.js ./src/assets/img/grafika/papeteria/tablice_rejestracyjne --outputDir ./src/assets/img/gallery/registrationTables --gName=registrationTables
cp -r ./src/assets/img/gallery/registrationTables ./dist/app/browser/assets/img/gallery/registrationTables
node ./node_modules/angular2-image-gallery/convert.js ./src/assets/img/grafika/papeteria/toppery --outputDir ./src/assets/img/gallery/toppers --gName=toppers
cp -r ./src/assets/img/gallery/toppers ./dist/app/browser/assets/img/gallery/toppers
node ./node_modules/angular2-image-gallery/convert.js ./src/assets/img/grafika/papeteria/winietki --outputDir ./src/assets/img/gallery/vignettes --gName=vignettes
cp -r ./src/assets/img/gallery/vignettes ./dist/app/browser/assets/img/gallery/vignettes
node ./node_modules/angular2-image-gallery/convert.js ./src/assets/img/grafika/papeteria/zaproszenia --outputDir ./src/assets/img/gallery/invitations --gName=invitations
cp -r ./src/assets/img/gallery/invitations ./dist/app/browser/assets/img/gallery/invitations
node ./node_modules/angular2-image-gallery/convert.js ./src/assets/img/grafika/papeteria/zawieszki --outputDir ./src/assets/img/gallery/pendants --gName=pendants
cp -r ./src/assets/img/gallery/pendants ./dist/app/browser/assets/img/gallery/pendants


node ./node_modules/angular2-image-gallery/convert.js ./src/assets/img/architektura/wnetrza/glubczyce --outputDir ./src/assets/img/gallery/glubczyce --gName=glubczyce
cp -r ./src/assets/img/gallery/glubczyce ./dist/app/browser/assets/img/gallery/glubczyce
node ./node_modules/angular2-image-gallery/convert.js ./src/assets/img/architektura/wnetrza/myslowice --outputDir ./src/assets/img/gallery/myslowice --gName=myslowice
cp -r ./src/assets/img/gallery/myslowice ./dist/app/browser/assets/img/gallery/myslowice
node ./node_modules/angular2-image-gallery/convert.js ./src/assets/img/architektura/wnetrza/tychy --outputDir ./src/assets/img/gallery/tychy --gName=tychy
cp -r ./src/assets/img/gallery/tychy ./dist/app/browser/assets/img/gallery/tychy
node ./node_modules/angular2-image-gallery/convert.js ./src/assets/img/architektura/wnetrza/ustron --outputDir ./src/assets/img/gallery/ustron --gName=ustron
cp -r ./src/assets/img/gallery/ustron ./dist/app/browser/assets/img/gallery/ustron

node ./node_modules/angular2-image-gallery/convert.js ./src/assets/img/architektura/meble --outputDir ./src/assets/img/gallery/furnitures --gName=furnitures
cp -r ./src/assets/img/gallery/furnitures ./dist/app/browser/assets/img/gallery/furnitures

echo KONIEC
